O:
This morning, I learned some basic knowledge about the front-end: HMTL, CSS, JS, and then learned a part of the React framework, as well as the syntax of JSX. This part of the content is relatively weak for me.
R:
unfamiliar
I:
I am not very familiar with the front-end, although the syntax is not very difficult, it leads to low efficiency due to unfamiliarity, and I am not very proficient in many React operations. For example, for today's homework, I need to click a button to obtain the input value, and I need to bind data in real time, but I don't know how to write the syntax, but I believe these can be gradually learned.
D:
Practice the front-end more and become familiar with the front-end syntax