import { Link } from "react-router-dom"
import { AppstoreOutlined, MailOutlined, SettingOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import { useState } from "react";

const Navigate = () => {

    const items = [
        {
            label: <Link to='/'>Home Page</Link>,
            key: 'home'
        },
        {
            label: <Link to='/help'>Help Page</Link>,
            key: 'help'
        },
        {
            label: <Link to='/done'>Done Page</Link>,
            key: 'done'
        },
    ]

    const [current, setCurrent] = useState('home');

    const onClick = (e) => {
        setCurrent(e.key)
    }

    return (
        <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />
    )
}

export default Navigate