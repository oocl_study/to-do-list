import ToDoItem from "./ToDoItem"
import "../style/ToDoGroup.css"
import { useSelector } from "react-redux"
import { List } from "antd"
const ToDoGroup = () => {

    const toDoItemList = useSelector(state => state.toDoReducer.toDoList)

    return (
        <List className="todoGroup"
            size="large"
            bordered
            dataSource={toDoItemList}
            renderItem={(item, index) =>
                <List.Item >
                    <ToDoItem key={index} toDoItem={item}></ToDoItem>
                </List.Item>}
        >
        </List>
    )
}

export default ToDoGroup