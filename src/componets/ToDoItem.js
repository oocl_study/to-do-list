import '../style/ToDoItem.css'
import useToDo from '../api/useToDo'
import { Button } from 'antd'
import { useEffect, useRef, useState } from 'react'

const ToDoItem = ({ toDoItem }) => {

    const { deleteData, updateData } = useToDo()
    const [canEdit, setCanEdit] = useState()
    const inputRef = useRef();
    const [todoMsg, setTodoMsg] = useState(toDoItem.text)


    useEffect(() => {
        const handleClickOutside = (e) => {
            if (canEdit && inputRef.current && !inputRef.current.contains(e.target)) {
                setCanEdit(false)
                updateData({
                    id: toDoItem.id,
                    text: todoMsg,
                    done: toDoItem.done
                })
            }
        };


        document.addEventListener('mousedown', handleClickOutside)

        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [canEdit, todoMsg, toDoItem, updateData]);



    const deleteItem = () => {
        deleteData(toDoItem.id)
    }

    const lineItem = () => {
        updateData({
            id: toDoItem.id,
            text: toDoItem.text,
            done: !toDoItem.done
        })
    }

    const editItem = () => {
        setCanEdit(true)
    }



    const handleInputChange = (e) => {
        setTodoMsg(e.target.value)
        console.log(e.target.value)
    }


    return (
        <div className='itemDiv'>
            {
                canEdit ? <input type='text' defaultValue={toDoItem.text} onChange={handleInputChange} ref={inputRef}></input>
                    : (<div className={toDoItem.done ? "lineDone" : "lineUnDone"} onClick={lineItem}>{toDoItem.text}</div>)
            }

            <Button type='primary' danger className='deleteButton' onClick={deleteItem}>x</Button>
            <Button type='primary' className='editButton' onClick={editItem}>edit</Button>
        </div>
    )
}

export default ToDoItem