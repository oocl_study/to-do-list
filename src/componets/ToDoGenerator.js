import { useState } from "react";
import "../style/ToDoGenerator.css"
import useToDo from "../api/useToDo";
import { Button, Input } from "antd";

const ToDoGenerator = () => {

    const [text, setText] = useState('')
    const {addData} = useToDo()

    const inputChange = (event) => {
        setText(event.target.value)
    }
   
    const addToDoItem = () => {
        if(text === '')return 
        addData(text)
    }
    
    return (
        <div className="todoGenerator">
            <Input className="input" showCount maxLength={100} onChange={inputChange}></Input>
            <Button type = "primary" onClick={addToDoItem}>add</Button>
        </div>
    )
}

export default ToDoGenerator