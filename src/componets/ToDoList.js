import ToDoGroup from "./ToDoGroup"
import ToDoGenerator from "./ToDoGenerator"

import { useEffect, useState } from 'react'
import { getTodos } from "../api/todo"
import { useDispatch } from "react-redux"

import useToDo from "../api/useToDo"

const ToDoList = () => {


    const {getData} = useToDo()

    useEffect(() => {
        getData()
    }, [])


    return (
        <div>
            <h1>TodoList</h1>
            <ToDoGroup ></ToDoGroup>
            <ToDoGenerator ></ToDoGenerator>
        </div>
    )
}

export default ToDoList