import { createSlice } from '@reduxjs/toolkit'

export const toDoListSlice = createSlice({
  name: 'toDoListSlie',
  initialState: {
    toDoList: [
      
    ]
  },
  reducers: {
    addToDo: (state, action) => {
        state.toDoList.push(action.payload)
    },
    deleteToDoItem: (state, action) => {
        state.toDoList = state.toDoList.filter(item => {
            return item.id !== action.payload
        })
    },
    updateToDoStatus: (state, action) => {
      console.log(action)
        const toDoItem = state.toDoList.find(item => {
            return item.id === action.payload.id
        })

        console.log(toDoItem)
        toDoItem.done = !toDoItem.done
    },
    initTodos: (status, action) => {
      status.toDoList = action.payload
    }
  },
})

export const { addToDo, deleteToDoItem, updateToDoStatus, initTodos } = toDoListSlice.actions

export default toDoListSlice.reducer