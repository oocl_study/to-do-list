import { configureStore } from "@reduxjs/toolkit";
import toDoReducer from "./toDoListSlice";

export default configureStore({
    reducer: {
        toDoReducer: toDoReducer
    }
  })