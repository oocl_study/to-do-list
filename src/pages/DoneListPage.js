import { List } from "antd"
import { useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"

import '../style/DoneListPage.css'

const DoneListPage = () => {
    const toDoList = useSelector(state => state.toDoReducer.toDoList).filter(todo => todo.done === true)

    const navigate = useNavigate()

    const toDetailPage = (id) => {
        console.log(id)
        navigate(`/done/${id}`)

    }

    return (
        <div>
            <h1>Done List</h1>
            <List
                size="large"
                bordered
                dataSource={toDoList}
                renderItem={(item) =>
                    <List.Item >
                        <div onClick={() => toDetailPage(item.id)} className="doneTextDiv"> { item.text }</div>
                    </List.Item>
                }
            >

            </List >

 
        </div >
    )
}

export default DoneListPage