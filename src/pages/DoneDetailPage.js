import { Table } from "antd"
import { useSelector } from "react-redux"
import { useParams } from "react-router-dom"
import useToDo from "../api/useToDo"
import { useEffect, useState } from "react"

const DoneDetailPage = () => {

    const { id } = useParams()

    const { getDataById } = useToDo()

    const [todoDetail, setTodoDetail] = useState({id: 1, text: "kkk"})

    const getTodoDetail = async() => {
        const data = await getDataById(id)
        setTodoDetail(data)
    }
    

    useEffect( () => {
        getTodoDetail()
    })

    const column = [
        {
            title: 'id',
            dataIndex: 'id'
        },
        {
            title: 'text',
            dataIndex: 'text'
        },
    ]

    const data = [
        {
            id: todoDetail.id,
            text: todoDetail.text
        }
    ]

    console.log(id)
    console.log("yy",todoDetail)

    return (
        <div>
            <h1>Done Detail</h1>
            <Table columns={column} dataSource={data} size="middle"></Table> 
        </div>
    )
}

export default DoneDetailPage