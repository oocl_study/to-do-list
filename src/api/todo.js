import axios from "axios"

const instance = axios.create({
    baseURL: 'https://64c0b69c0d8e251fd1126615.mockapi.io/',
})

export const getTodos = () => {
    return instance.get('/todos')
}

export const createTodo = (text) => {
    return instance.post('/todos', {
        done: false,
        text
    })
}

export const deleteTodo = (id) => {
    return instance.delete(`/todos/${id}`)
}

export const updateTodo = (toDoItem) => {
    return instance.put(`/todos/${toDoItem.id}`, {
        text: toDoItem.text,
        done: toDoItem.done
    })
}