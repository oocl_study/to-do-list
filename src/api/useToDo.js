import { useDispatch } from "react-redux"
// import { createTodo, deleteTodo, getTodos, updateTodo } from "./todo"
import { createTodo, deleteTodo, getTodoById, getTodos, updateTodo } from "./api"
import { addToDo, deleteToDoItem, initTodos, updateStatus, updateToDoStatus, updateToDoStatusd } from "../app/toDoListSlice"

const useToDo = () => {

    const dispatch = useDispatch()

    const getData = async () => {
        const response = await getTodos()
        dispatch(initTodos(response.data))
    }

    const addData = async (text) => {
        const {data} = await createTodo(text)
        dispatch(addToDo(data))
    }
    const deleteData = async (id) => {
        await deleteTodo(id)
        dispatch(deleteToDoItem(id))
    } 
    const updateData = async(toDoItem) => {
        // console.log(toDoItem)
        await updateTodo(toDoItem)
        getData()
    }
    const getDataById = async(id) => {
        const {data} = await getTodoById(id)
        return data
    }

    

    return {
        getData, addData, deleteData, updateData, getDataById
    }
}

export default useToDo
