import axios from "axios"

const instance = axios.create({
    baseURL: 'http://localhost:8080/',
})

export const getTodos = () => {
    return instance.get('/todos')
}

export const createTodo = (text) => {
    return instance.post('/todos', {
        done: false,
        text
    })
}

export const deleteTodo = (id) => {
    return instance.delete(`/todos/${id}`)
}

export const updateTodo = (toDoItem) => {
    return instance.put(`/todos/${toDoItem.id}`, {
        text: toDoItem.text,
        done: toDoItem.done
    })
}

export const getTodoById = (id) => {
    return instance.get(`/todos/${id}`)
}